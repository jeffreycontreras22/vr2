// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VR_DevGameMode.generated.h"

UCLASS(minimalapi)
class AVR_DevGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVR_DevGameMode();
};



