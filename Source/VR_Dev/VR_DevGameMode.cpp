// Copyright Epic Games, Inc. All Rights Reserved.

#include "VR_DevGameMode.h"
#include "VR_DevHUD.h"
#include "VR_DevCharacter.h"
#include "UObject/ConstructorHelpers.h"

AVR_DevGameMode::AVR_DevGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AVR_DevHUD::StaticClass();
}
